<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api','cors']], function () {
    /*
    * Users
    */
    Route::get('users', "UserController@list");
    Route::get('users/{id}', "UserController@show");
    Route::post('users', "UserController@store");
    Route::post('users/{id}/delete', "UserController@delete");
    /*
    * Branches
    */
    Route::get('branches', "BranchController@list");
    Route::get('branches/{id}', "BranchController@show");
    Route::post('branches', "BranchController@store");
    Route::post('branches/{id}/delete', "BranchController@delete");
    /*
    * Bundles
    */
    Route::get('bundles', "BundleController@list");
    Route::get('bundles/{id}', "BundleController@show");
    Route::post('bundles', "BundleController@store");
    Route::post('bundles/{id}/delete', "BundleController@delete");
    /*
    * Schedules
    */
    Route::get('schedules', "ScheduleController@list");
    Route::get('schedules/{id}', "ScheduleController@show");
    Route::post('schedules', "ScheduleController@store");
    Route::post('schedules/{id}/delete', "ScheduleController@delete");
    /*
    * Subscriptions
    */
    Route::get('subscriptions', "SubscriptionController@list");
    Route::get('subscriptions/{id}', "SubscriptionController@show");
    Route::post('subscriptions', "SubscriptionController@store");
    Route::post('subscriptions/{id}/delete', "SubscriptionController@delete");
    /*
    * Clients
    */
    Route::get('clients', "ClientController@list");
    Route::get('clients/{id}', "ClientController@show");
    Route::post('clients', "ClientController@store");
    Route::post('clients/{id}/delete', "ClientController@delete");
    /*
    * Custom fields
    */
    Route::get('custom-fields', "CustomFieldController@list");
    Route::get('custom-fields/{id}', "CustomFieldController@show");
    Route::post('custom-fields', "CustomFieldController@store");
    Route::post('custom-fields/{id}/delete', "CustomFieldController@delete");
    /*
    * Journals
    */
    Route::get('journals', "JournalController@list");
    Route::get('journals/{id}', "JournalController@show");
    Route::get('journals/client/{client_id}',"JournalController@showClient");
    Route::post('journals', "JournalController@store");
    Route::post('journals/{id}/delete', "JournalController@delete");
    Route::post('journals/client/{client_id}/delete','JournalController@deleteClient');
    /*
    * Client subscriptions
    */
    Route::get('client-subscriptions', "ClientSubscriptionController@list");
    Route::get('client-subscriptions/{id}', "ClientSubscriptionController@show");
    Route::post('client-subscriptions', "ClientSubscriptionController@store");
    Route::post('client-subscriptions/{id}/delete', "ClientSubscriptionController@delete");
    /*
    * Group trainings
    */
    Route::get('group-trainings', "GroupTrainingController@list");
    Route::get('group-trainings/{id}', "GroupTrainingController@show");
    Route::post('group-trainings', "GroupTrainingController@store");
    Route::post('group-trainings/{id}/delete', "GroupTrainingController@delete");
    /*
     * Pass
     */
    Route::post('pass','PassController@pass');
});

Route::group(['middleware' => ['cors']], function () {
         /*
         * Login
         */
    Route::post('login', "LoginController@login");
});
