<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->string('subscription_name');
            $table->string('bundle_name');
            $table->string('visiting_condition_name');
            $table->string('visiting_condition_number_of_visits');
            $table->string('visiting_condition_days');
            $table->string('schedule_name');
            $table->time('schedule_time_start');
            $table->time('schedule_time_end');
            $table->string('schedule_days_of_week');
            $table->date('date_start')->default(\Carbon\Carbon::now());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_subscriptions');
    }
}
