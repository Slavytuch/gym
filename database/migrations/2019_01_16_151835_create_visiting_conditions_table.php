<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitingConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visiting_conditions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('number_of_visits')->default(-1);
            $table->integer('subscription_id')->unsigned();
            $table->integer('days')->default(0)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visiting_conditions');
    }
}
