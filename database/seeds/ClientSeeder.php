<?php

use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            [
                "name" => "Имя клиента 1",
                "date_of_birth" => "1990-01-01",
                "card_number" => "Карта клиента 1"
            ],
            [
                "name" => "Имя клиента 2",
                "date_of_birth" => "1990-01-01",
                "card_number" => "Карта клиента 2"
            ]
        ]);
        DB::table('clients')->insert([
            [
                "name" => "Имя клиента 3",
                "date_of_birth" => "1990-01-01",
                "card_number" => "Карта клиента 3"
            ],
            [
                "name" => "Имя клиента 4",
                "date_of_birth" => "1990-01-01",
                "card_number" => "Карта клиента 4"
            ],
            [
                "name" => "Имя клиента 5",
                "date_of_birth" => "1990-01-01",
                "card_number" => "Карта клиента 5"
            ]
        ]);
    }
}
