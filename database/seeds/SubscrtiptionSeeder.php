<?php

use Illuminate\Database\Seeder;

class SubscrtiptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscriptions')->insert([
            [
                'name' => 'Имя услуги 1',
                'schedule_id' => 1
            ],
            [
                'name' => 'Имя услуги 2',
                'schedule_id' => 2
            ],
            [
                'name' => 'Имя услуги 3',
                'schedule_id' => 2
            ],
        ]);
    }
}
