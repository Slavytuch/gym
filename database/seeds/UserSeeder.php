<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                "name" => "Имя 1",
                "email" => "whatever@email.com",
                "password" => bcrypt("HelloWorld321"),
                "remember_token" => str_random(10)
            ]
        ]);

        DB::table('oauth_clients')->insert([
            [
                "user_id" => 1,
                "name" => "Laravel Personal Access Client",
                "secret" => "guTdhDH0kTtVp0gpU6mImzLM6EmB8okkcVq1T5i7",
                "redirect" => "http://localhost",
                "personal_access_client" => 1,
                "password_client" => 0,
                "revoked" => 0
            ]
        ]);

        DB::table('oauth_personal_access_clients')->insert([
            [
                "client_id" => 1,
            ]
        ]);
    }
}
