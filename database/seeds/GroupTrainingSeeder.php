<?php

use Illuminate\Database\Seeder;

class GroupTrainingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('group_trainings')->insert([
            [
                "name" => "Имя группового занятия 1",
                "colour_name" => "RED",
                "colour_hex" => "#ff0000",
                "schedule_id" => 1
            ],
            [
                "name" => "Имя группового занятия 2",
                "colour_name" => "GREEN",
                "colour_hex" => "#008000",
                "schedule_id" => 2
            ],
        ]);
    }
}
