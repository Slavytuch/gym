<?php

use Illuminate\Database\Seeder;

class VisitingConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('visiting_conditions')->insert([
            [
                "name" => "Имя условий посещения 1",
                "subscription_id"=>1
            ],
            [
                "name" => "Имя условий посещения 2",
                "subscription_id"=>1
            ],
            [
                "name" => "Имя условий посещения 3",
                "subscription_id"=>2
            ],
        ]);
    }
}
