<?php

use Illuminate\Database\Seeder;

class BundleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bundles')->insert([
            [
                "name" => "Имя группы сервисов 1"
            ],
            [
                "name" => "Имя группы сервисов 2"
            ],
            [
                "name" => "Имя группы сервисов 3"
            ],
        ]);
    }
}
