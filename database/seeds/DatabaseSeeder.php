<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             UserSeeder::class,
             BranchSeeder::class,
             BundleSeeder::class,
             ScheduleSeeder::class,
             VisitingConditionSeeder::class,
             SubscrtiptionSeeder::class,
             ClientSeeder::class,
             CustomFieldSeeder::class,
             JournalSeeder::class,
             ClientSubscriptionSeeder::class,
             GroupTrainingSeeder::class,
             BranchSubscriptionSeeder::class,
         ]);
    }
}
