<?php

use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branches')->insert([
            [
                "name" => "Имя филиала 1"
            ],
            [
                "name" => "Имя филиала 2"
            ],
            [
                "name" => "Имя филиала 3"
            ]
        ]);
    }
}
