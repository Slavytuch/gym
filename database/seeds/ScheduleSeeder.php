<?php

use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schedules')->insert([
            [
                "name" => "Имя расписания 1"
            ],
            [
                "name" => "Имя расписания 2"
            ],
            [
                "name" => "Имя расписания 3"
            ],
        ]);
    }
}
