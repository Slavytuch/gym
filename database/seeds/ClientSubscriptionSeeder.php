<?php

use Illuminate\Database\Seeder;

class ClientSubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('client_subscriptions')->insert([
            [
                "client_id" => 1,
                "subscription_name" => "Имя подписки клиента 1",
                "bundle_name"=>"Имя группы подписок клиента 1",
                "visiting_condition_name"=>"Имя условий посещения клиента 1",
                "visiting_condition_number_of_visits"=>-1,
                "visiting_condition_days"=>0,
                "schedule_name"=>"Имя расписания клиента 1",
                "schedule_time_start"=>'9:00',
                "schedule_time_end"=>'21:00',
                "schedule_days_of_week"=>"12345"
            ],
            [
                "client_id" => 1,
                "subscription_name" => "Имя подписки клиента 2",
                "bundle_name"=>"Имя группы подписок клиента 1",
                "visiting_condition_name"=>"Имя условий посещения клиента 2",
                "visiting_condition_number_of_visits"=>1,
                "visiting_condition_days"=>2,
                "schedule_name"=>"Имя расписания клиента 1",
                "schedule_time_start"=>'9:00',
                "schedule_time_end"=>'21:00',
                "schedule_days_of_week"=>"60"
            ],
            [
                "client_id" => 2,
                "subscription_name" => "Имя подписки клиента 3",
                "bundle_name"=>"Имя группы подписок клиента 3",
                "visiting_condition_name"=>"Имя условий посещения клиента 3",
                "visiting_condition_number_of_visits"=>-1,
                "visiting_condition_days"=>365,
                "schedule_name"=>"Имя расписания клиента 2",
                "schedule_time_start"=>'10:00',
                "schedule_time_end"=>'23:00',
                "schedule_days_of_week"=>"1234560"
            ],
        ]);
    }
}
