<?php

use Illuminate\Database\Seeder;

class BranchServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branch_service')->insert([
            [
                'branch_id' => 1,
                'service_id' => 1
            ],
            [
                'branch_id' => 1,
                'service_id' => 2
            ],
            [
                'branch_id' => 2,
                'service_id' => 1
            ],
            [
                'branch_id' => 3,
                'service_id' => 3
            ],
            [
                'branch_id' => 3,
                'service_id' => 4
            ]
        ]);
    }
}
