<?php

use Illuminate\Database\Seeder;

class CustomFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('custom_fields')->insert([
            [
                "name" => "Имя дополнительного поля 1",
                "value" => "Значение дополнительного поля 1",
                "client_id" => 1
            ],
            [
                "name" => "Имя дополнительного поля 2",
                "value" => "Значение дополнительного поля 2",
                "client_id" => 2
            ]
        ]);
    }
}
