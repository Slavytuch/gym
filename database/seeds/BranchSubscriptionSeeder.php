<?php

use Illuminate\Database\Seeder;

class BranchSubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branch_subscription')->insert([
            [
                'branch_id' => 1,
                'subscription_id' => 1
            ],
            [
                'branch_id' => 1,
                'subscription_id' => 2
            ],
            [
                'branch_id' => 2,
                'subscription_id' => 1
            ],
            [
                'branch_id' => 3,
                'subscription_id' => 3
            ],
            [
                'branch_id' => 3,
                'subscription_id' => 4
            ]
        ]);
    }
}
