<?php

use Illuminate\Database\Seeder;

class JournalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('journals')->insert([
            [
                "date" => "2019-01-17",
                "client_subscription_id" => 1,
                "client_id" => 1
            ],
            [
                "date" => "2019-01-17",
                "client_subscription_id" => 2,
                "client_id" => 2
            ],
            [
                "date" => "2019-01-17",
                "client_subscription_id" => 3,
                "client_id" => 4
            ]
        ]);
    }
}
