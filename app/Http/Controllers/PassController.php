<?php

namespace App\Http\Controllers;

use App\Models\Journal;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PassController extends Controller
{
    protected $rules = [
        "client_id" => "required",
        "subscription_id" => "required"
    ];

    public function pass(Request $request)
    {
        $request->validate($this->rules);
        $list = \App\Models\ClientSubscription::where('client_id', $request->client_id)->where('id', $request->subscription_id)->get();
        if ($list->count() == 0) {
            return ['success' => false, 'message' => 'Client or subscription not found'];
        }
        $list = $list[0];
        if ($list->available && $list->usable) {
            $currentEntry = Journal::where('date', '=', Carbon::now()->toDateString())->get();
            if ($currentEntry->count() == 0 || isset($currentEntry[$currentEntry->count() - 1]['time_exit'])) {
                Journal::create(['client_id' => $request->client_id, 'client_subscription_id' => $request->subscription_id]);
            } else {
                Journal::updateOrCreate(['id' => $currentEntry[$currentEntry->count() - 1]['id']], ['client_id' => $request->client_id, 'client_subscription_id' => $request->subscription_id, 'time_exit' => Carbon::now()->toTimeString()]);
            }
        } else  return ['success' => false, 'message' => 'Subscription not usable or available'];
        return ['success' => true];
    }
}
