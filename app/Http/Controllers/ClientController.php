<?php

namespace App\Http\Controllers;

use App\Models\CustomField;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ClientController extends EntityController
{
    protected $entity = "\App\Models\Client";
    protected $rules = [
        'name' => 'required',
        'date_of_birth' => 'required',
        'card_number' => 'required',
    ];
    protected $relations = [
        'customFields',
        'subscriptions',
        'journal'
        ];

    protected function saveRelation(Request $request, $entity)
    {
        $entity->customFields()->delete();
        $entity->customFields()->saveMany(collect($request->custom_fields)->map(function ($item) use ($entity) {
            return new CustomField([
                'client_id' => $entity->id,
                'name' => $item['name'],
                'value' => $item['value']
            ]);
        }));
    }

    protected function doFilter(Request $request, $query)
    {
        if ($request->has('name')) {
            $query = $query->where('name', 'LIKE', '%' . $request->name . '%');
        }

        if ($request->has('card_number')) {
            $query = $query->where('card_number', 'LIKE', '%' . $request->card_number . '%');
        }

        if ($request->has('phone_number')) {
            $query = $query->where('phone_number', 'LIKE', '%' . $request->phone_number . '%');
        }

        return $query;
    }
}
