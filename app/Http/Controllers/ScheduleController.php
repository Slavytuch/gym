<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ScheduleController extends EntityController
{
    protected $entity = 'App\Models\Schedule';
    protected $rules = ['name' => 'required'];
}
