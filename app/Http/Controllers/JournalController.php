<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class JournalController extends EntityController
{
    protected $entity = "\App\Models\Journal";
    protected $rules = ['client_subscription_id' => 'required', 'client_id' => 'required'];
    protected $relations = ['subscription'];

    protected function doFilter(Request $request, $query)
    {
        if ($request->has('date')) {
            $query = $query->whereDate('date', '=', $request->date);
        }

        return $query;
    }

    public function showClient($client_id)
    {
        return $this->entity::where('client_id', '=', $client_id)->with($this->relations)->get();
    }

    public function deleteClient($client_id)
    {
        $this->entity::where('client_id', '=', $client_id)->delete();
        return ["success" => true];
    }

}
