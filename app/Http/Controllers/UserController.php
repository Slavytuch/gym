<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends EntityController
{
    protected $entity = '\App\Models\User';
    protected $rules = ['email' => 'required', 'password' => 'required', 'name' => 'required'];
}
