<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EntityController extends Controller
{
    protected $entity;
    protected $relations = [];
    protected $filters = [];
    protected $limit = 10;
    protected $order = 'id';
    protected $direction = 'asc';
    protected $rules = [];

    public function list(Request $request)
    {
        $query = $this->entity::with($this->relations);
        $query = $this->doFilter($request, $query);

        $query = $query->orderBy($this->getOrder($request), $this->getDirection($request));

        if ($this->paginationNeeded($request)) {
            $result = $query->paginate($this->calculateLimit($request));
        } else {
            $result = $query->get();
        }

        return $result;
    }

    public function show($id)
    {
        return $this->entity::with($this->relations)->find($id);
    }

    public function store(Request $request)
    {
        $request->validate($this->rules);
        $entity = $this->entity::updateOrCreate(['id' => $request->id], $request->all());
        $this->saveRelation($request, $entity);
        return ['success' => true, 'entity' => $entity];
    }

    public function delete($id)
    {
        $this->entity::find($id)->delete();
        return ["success" => true];
    }

    protected function saveRelation(Request $request, $entity)
    {

    }

    protected function doFilter(Request $request, $query)
    {
        return $query;
    }

    private function calculateLimit(Request $request)
    {
        return $request->has('limit') ? $request->limit : $this->limit;
    }

    private function paginationNeeded(Request $request): bool
    {
        return !$request->has('unlimited');
    }

    private function getDirection(Request $request)
    {
        return $request->has('direction') ? $request->direction : $this->direction;
    }

    private function getOrder(Request $request)
    {
        return $request->has('order') ? $request->order : $this->order;
    }



}