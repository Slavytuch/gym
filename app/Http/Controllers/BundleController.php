<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BundleController extends EntityController
{
    protected $entity = '\App\Models\Bundle';
    protected $rules = ['name' => 'required'];

    protected function doFilter(Request $request, $query)
    {
        if ($request->has('name')) {
            $query = $query->where('name', 'LIKE', '%' . $request->name . '%');
        }

        return $query;
    }
}
