<?php

namespace App\Http\Controllers;

use App\Models\VisitingCondition;
use Illuminate\Http\Request;

class SubscriptionController extends EntityController
{
    protected $entity = '\App\Models\Subscription';
    protected $relations = ['visitingConditions', 'schedule', 'bundle'];
    protected $rules = [
        'name' => 'required',
        'schedule_id' => 'required',
    ];

    protected function saveRelation(Request $request, $entity)
    {
        $entity->visitingConditions()->delete();
        $entity->visitingConditions()->saveMany(collect($request->visiting_conditions)->map(function ($item) use ($entity) {
            return new VisitingCondition([
                'subscription_id' => $entity->id,
                'name' => $item['name'],
                'number_of_visits' => isset($item['number_of_visits']) ? $item['number_of_visits'] : null,
                'days' => isset($item['days']) ? $item['days'] : null,
            ]);
        }));
    }
}
