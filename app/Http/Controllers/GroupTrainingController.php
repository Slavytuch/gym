<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GroupTrainingController extends EntityController
{
    protected $entity = "\App\Models\GroupTraining";
    protected $rules = ['name' => 'required', 'colour_name' => 'required', 'colour_hex' => 'required',];
    protected $relations = ['schedule'];

    protected function doFilter(Request $request, $query)
    {
        if ($request->has('name')) {
            $query = $query->where('name', 'LIKE', '%' . $request->name . '%');
        }

        if ($request->has('colour')) {
            $query = $query->where('colour_name', 'LIKE', '%' . $request->colour . '%');
        }

        if ($request->has('time')) {
            $query = $query->whereTime('time_start', '<=', $request->time)->whereTime('time_end', '>=', $request->time);
        }

        if ($request->has('date')) {
            $query = $query->whereDate('date', '=', $request->date);
        }

        return $query;
    }
}
