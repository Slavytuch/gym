<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomFieldController extends EntityController
{
    protected $entity = '\App\Models\CustomField';
    protected $rules = ['name' => 'required', 'client_id' => 'required'];
}
