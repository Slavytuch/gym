<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BranchController extends EntityController
{
    protected $entity = '\App\Models\Branch';
    protected $rules = ['name' => 'required'];
    protected $relations = ['subscriptions'];

    protected function saveRelation(Request $request, $entity)
    {
        $entity->subscriptions()->sync(collect($request->subscriptions)->map(function ($subscription) {
            return isset($subscription["id"]) ? $subscription["id"] : $subscription;
        }));
    }

    protected function doFilter(Request $request, $query)
    {
        if ($request->has('name')) {
            $query = $query->where('name', 'LIKE', '%' . $request->name . '%');
        }

        return $query;
    }
}
