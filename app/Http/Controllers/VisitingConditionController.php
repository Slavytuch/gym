<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VisitingConditionController extends EntityController
{
    protected $entity = "\App\Models\VisitingCondition";
    protected $rules = ['name' => 'required', 'subscription_id' => 'required'];
}
