<?php

namespace App\Http\Controllers;

use App\Models\Journal;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ClientSubscriptionController extends EntityController
{
    protected $entity = "\App\Models\ClientSubscription";
    protected $rules = [
        "client_id" => "required",
        "subscription_name" => "required",
        "bundle_name" => "required",
        "visiting_condition_name" => "required",
        "visiting_condition_number_of_visits" => "required",
        "visiting_condition_days" => "required",
        "schedule_name" => "required",
        "schedule_time_start" => "required",
        "schedule_time_end" => "required",
        "schedule_days_of_week" => "required",
    ];
    protected $relations = [];



}
