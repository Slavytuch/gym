<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    protected $fillable = ['id', 'date', 'client_subscription_id', 'client_id', 'time_entry', 'time_exit'];

    public function subscription()
    {
        return $this->belongsTo('App\Models\ClientSubscription', 'client_subscription_id');
    }
}
