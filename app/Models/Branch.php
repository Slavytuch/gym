<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['id', 'name', 'status', 'sort_order'];

    public function subscriptions(){
        return $this->belongsToMany('\App\Models\Subscription');
    }
}
