<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    protected $fillable = ['id', 'name', 'date_of_birth', 'card_number', 'phone_number'];

    public function customFields()
    {
        return $this->hasMany('\App\Models\CustomField');
    }

    public function subscriptions()
    {
        return $this->hasMany('\App\Models\ClientSubscription');
    }

    public function journal(){
        return $this->hasMany('\App\Models\Journal');
    }

}
