<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VisitingCondition extends Model
{
    protected $fillable = ['id', 'name', 'number_of_visits', 'days'];
}
