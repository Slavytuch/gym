<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ClientSubscription extends Model
{
    protected $fillable = [
        'id',
        "client_id",
        "subscription_name",
        "bundle_name",
        "visiting_condition_name",
        "visiting_condition_number_of_visits",
        "visiting_condition_days",
        "schedule_name",
        "schedule_time_start",
        "schedule_time_end",
        "schedule_days_of_week"
    ];

    protected $appends = ['available', 'usable', 'uses_left'];

    protected function getAvailableAttribute()
    {
        $date_start = new Carbon($this["date_start"]);
        $date_end = $date_start->addDays($this['visiting_condition_days']);
        $result = true;
        if ($date_start != $date_end || ($date_start <= Carbon::now() && $date_end >= Carbon::now())) {
            //search by days
            $result = false;
        }

        $currentNumberOfVisits = Journal::where('client_id', '=', $this['client_id'])->where('client_subscription_id', '=', $this['id'])->get()->count();
        if ($this['visiting_condition_number_of_visits'] != -1 && $currentNumberOfVisits >= $this['visiting_condition_number_of_visits']) {
            //search by number of visits
            $result = false;
        }

        return $result;
    }

    protected function getUsableAttribute()
    {
        $current_time = Carbon::now()->toTimeString();
        $result = true;

        if ($current_time < $this['schedule_time_start'] || $current_time > $this['schedule_time_end']) {
            //check current time
            $result = false;
        }

        if (strpos($this['schedule_days_of_week'], (string)Carbon::now()->dayOfWeek) == false) {
            //check current day of week
            $result = false;
        }

        return $result;
    }

    protected function getUsesLeftAttribute()
    {
        return $this['visiting_condition_number_of_visits'] == -1 ? "Unlimited" :
            $this['visiting_condition_number_of_visits'] - Journal::where('client_id', '=', $this['client_id'])->where('client_subscription_id', '=', $this['id'])->get()->count();
    }

}
