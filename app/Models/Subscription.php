<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = ['id', 'name', 'visiting_condition_id', 'schedule_id', 'price', 'bundle_id'];

    public function visitingConditions()
    {
        return $this->hasMany('\App\Models\VisitingCondition');
    }

    public function schedule()
    {
        return $this->belongsTo('\App\Models\Schedule');
    }

    public function bundle()
    {
        return $this->belongsTo('\App\Models\Bundle');
    }


}
