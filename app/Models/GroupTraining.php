<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupTraining extends Model
{
    protected $fillable = ['id', 'name', 'colour_name', 'colour_hex', 'schedule_id'];

    public function schedule(){
        return $this->belongsTo('\App\Models\Schedule');
    }
}
